
const black = "#373737"
const red = "#ac0800"
const green = "#5a9216"
const btn_play = document.getElementById('btn-play')
const saldo = document.getElementById('saldo')
const saldo_selected = document.getElementById('saldo-selected')
const color_selected = document.getElementById('color-selected')
const ok_confirm = document.getElementById('ok-confirm')
const btn_confirm = document.querySelector('.btn-confirm')
let startGame = false

const player = {saldo: 20000, name: 'usuario prueba'}

saldo.textContent = `$${player.saldo}`

const confirmInput = () => {

  if (saldo_selected.value > player.saldo || saldo_selected.value <= 500) {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'El saldo que debes ingresar debe ser mayor a 500 y menor a tu saldo actual!',
    })
  }
  if (saldo_selected.value && color_selected.value) {
    btn_play.disabled = false
    btn_confirm.disabled = true
    ok_confirm.classList.remove('d-none')
  } else {
    Swal.fire('Rellena los campos!')
  }
  

}

let theWheel = new Winwheel({
  'numSegments': 31,
  "textFontSize": 6,
  'innerRadius': 10,
  'textAlignment': 'center',
  'segments':
    [
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white'},
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'},
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white'},
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'}, 
      { 'fillStyle': red, 'text': 'r', 'textFillStyle': 'white' },
      { 'fillStyle': black, 'text': 'n', 'textFillStyle': 'white'},
      { 'fillStyle': green, 'text': 'v', 'textFillStyle': 'white' },

    ],
  'animation':                   // Note animation properties passed in constructor parameters.
    {
      'type': 'spinToStop',  
      'duration': 5,           
      'spins': 2,
      'callbackSound': playSound,
      // se ejecuta cuando termina de girar
      'callbackFinished': 'alertPrize()',
      // Durante la animación es necesario llamar a drawTriangle () para volver a dibujar el puntero en cada cuadro.
      'callbackAfter': 'drawTriangle()'
    }
});

const playGame = () => {
  startGame = true;
  btn_play.disabled = true;
  theWheel.startAnimation();
}

let audio = new Audio('../media/tick.mp3');  // Create audio object and load desired file.

function playSound() {
  // Stop and rewind the sound (stops it if already playing).
  audio.pause();
  audio.currentTime = 0;

  // Play the sound.
  audio.play();
}

function alertPrize() {
 
  let colorWin = theWheel.getIndicatedSegment();

  if (color_selected.value === colorWin.text){
    Swal.fire(
      'Buen trabajo!',
      `Has ganado! $ ${saldo_selected.value}`,
      'success'
    )
    player.saldo += parseInt(saldo_selected.value)

    
  } else {
    Swal.fire({
      icon: 'error',
      title: 'Oops...',
      text: 'Perdistes!',
    })
    player.saldo -= parseInt(saldo_selected.value)
  }
  console.log('new value ', player.saldo)
  // update view saldo
  saldo.textContent = `$${player.saldo}`
  

  
}
// Function to draw pointer using code (like in a previous tutorial).
drawTriangle();

function drawTriangle() {
  // Get the canvas context the wheel uses.
  let ctx = theWheel.ctx;
  ctx.moveTo(170, 5);           // Move to initial position.
  ctx.lineTo(200, 40);
  ctx.lineTo(171, 5);
}

function resetWheel() {
  theWheel.stopAnimation(false);  // Stop the animation, false as param so does not call callback function.
  theWheel.rotationAngle = 0;     // Re-set the wheel angle to 0 degrees.
  theWheel.draw();   
  btn_confirm.disabled = false             
  wheelSpinning = true;         

  if (startGame) {
    btn_play.disabled = false
  }
}





